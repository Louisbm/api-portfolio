https://www.freecodecamp.org/news/how-to-write-a-good-readme-file/

// GET       /api/v1/projects
// GET       /api/v1/projects/:id 
// POST     /api/v1/login

// POST     /api/v1/logout
// POST     /api/v1/projects
// GET      /api/v1/technos
// PATCH    /api/v1/projects/:id
// DELETE    /api/v1/projects/:id

// GET       /api/v1/cv
// GET       /api/v1/cv/about
// GET       /api/v1/cv/professional
// GET       /api/v1/cv/academic
// GET       /api/v1/cv/languages
// GET       /api/v1/cv/hobbies


## GET       /api/v1/projects/:lang
{
    status: 'success',
    data: {
        projectsNumber: 4,
        projects: [
            {
                title: 'Project title',
                description: 'Project description',
                url: 'Project url',
                repository: 'Repository url',
                technos: [
                    'PHP', 'Node', 'Sass', 'Redis', 'MySQL', 'Nginx', 'Socket.io', 'Google Maps API'
                ]
            },
            {
                title: 'Project title',
                description: 'Project description',
                url: 'Project url',
                repository: 'Repository url',
                technos: [
                    'PHP', 'Node', 'Sass', 'Redis', 'MySQL', 'Nginx', 'Socket.io', 'Google Maps API'
                ]
            }
        ]
    }
}


## GET       /api/v1/projects/:lang/:id
{
    status: 'success',
    data: {
        project: {
            id: 45,
            title: '',
            description: '',
            url: '',
            repository: '',
            technos: [
                '', '', ''
            ]
        }
    }
}


## POST      /api/v1/projects
{ : REQUEST
    title: '',
    description: '',
    url: '',
    repository: '',
    technos: [
        '', '', ''
    ]
}

{ : RESPONSE
    status: 'success',
    data: {
        message: 'Projet ajouté!'
    }
}


## PATCH     /api/v1/projects/:id
{ : REQUEST
    id: 45,
    title: '',
    description: '',
    url: '',
    repository: '',
    technos: [
        '', '', ''
    ]
}

{ : RESPONSE
    status: 'success',
    data: {
        message: 'Projet modifié!'
    }
}


## DELETE    /api/v1/projects/:id
{ : REQUEST
    id: 45
}

{ : RESPONSE
    status: 'success',
    data: {
        message: 'Projet supprimé!'
    }
}


## POST      /api/v1/login
{ : REQUEST
    email: '',
    password: ''
}

{ : RESPONSE
    status: 'success'
}


## GET       /api/v1/cv
{
    status: 'success',
    data: {
        cv: {
            about: {
                firstName: 'Louis',
                lastName: 'DANIEL',
                job: 'Backend Developer NodeJS',
                portfolio: 'https://danielouis.com',
                email: 'louis_daniel@outlook.fr',
                phone: '+33 7 81 21 25 90',
                gitlab: 'https://gitlab.com/Louisbm',
                linkedin: 'https://www.linkedin.com/in/louis-daniel-860093178',
                driverLicense: true
            },
            professional: [
                {
                    type: 'Volunteering',
                    jobTitle: 'Volunteer',
                    company: 'American Institude of Behavioral Research & Technology',
                    startDate: 2022-02-01,
                    endDate: null,
                    description: 'The AIBRT is a non profit, nonpartisan organization founded in 2012 and based in Vista, California, USA. It conducts and promotes research tht has the potential to increase the well-being and functioning of people worldwide. It currently has ongoing research projects in twelve different topic areas.'
                },
                {
                    type: 'Freelance',
                    jobTitle: 'Backend Developer NodeJS',
                    company: 'self'
                    startDate: 2022-01-01,
                    endDate: 2022-03-07,
                    description: 'Creating website for companies like Iso Plac Deco, a plasterer that wanted to redesign his landing page. Mainly programming with Javascript (NodeJS) with a little bit of VueJS.',
                },
                {
                    type: 'Permanent',
                    jobTitle: 'Founder',
                    company: 'Speache',
                    startDate: 2020-11-01,
                    endDate: null,
                    description: 'Speache is a rental service based on sharing economy. I created the application with NodeJS, PHP, MySQL and Redis for server side.'
                },
                {
                    type: 'Apprenticeship',
                    jobTitle: 'Quantitative Analyst in Credit Risk IFRS9',
                    company: 'BNP Paribas',
                    startDate: 2019-09-01,
                    endDate: 2020-09-01,
                    description: 'At BNP Paribas, my main missions were to produce the expected loss on the overseas perimeter. To do it, I used SAS and SQL to perform data analysis on the BNP Paribas database. I created and maintained existing algorithms, created the associated documentation and more.'
                },
                {
                    type: 'Volunteering',
                    jobTitle: 'President',
                    company: 'Association Étudiante Rennaise des Ingénieurs Économiques et Financiers (AERIEF)',
                    startDate: 2019-09-01,
                    endDate: 2020-09-01,
                    description: 'While i was president, I planned different events, was the link between students, teachers and professionals and did several administrative tasks.'
                },
                {
                    type: 'Internship',
                    jobTitle: 'Support for the development and the performance of the salesforce',
                    company: 'Societe Generale',
                    startDate: '2019-06-01',
                    endDate: '2019-08-31',
                    description: 'Statistical monitoring of commercial operations, Internal communication of the performance of 95 sales representatives, VBA development, Operational assistance of the salesforce...'
                }
            ],
            academic: {
                degree: 'Master',
                major: 'Financial Engineering',
                university: 'Faculty of economics - University of Rennes 1',
                city: 'Rennes',
                country: 'France'
                startDate: '2018-09-01',
                endDate: '2020-09-01'
            },
            languages: [
                {
                    name: 'French',
                    level: '100%'
                },
                {
                    name: 'English',
                    level: 80%
                },
                {
                    name: 'Bulu',
                    level: '50%'
                },
                {
                    name: 'Spanish',
                    level: '25%'
                }
            ],
            hobbies: [
                {
                    topic: 'Sport',
                    description: 'Football, Cardio training'
                },
                {
                    topic: 'New technologies',
                    description: 'VR, AR, Fintech'
                },
                {
                    topic: 'Personal development',
                    description: 'Dale Carnegie : How to develop self confidence and influence people by public speaking'
                },
                {
                    topic: 'Mangas',
                    description: 'I mainly like to watch animated version of mangas, like Shingeki No Kyojin, Demon Slayer for the most popular ones'
                }
            ]       
        }
    }
}



cv: {
    about: {
        firstName: 'Louis',
        lastName: 'DANIEL',
        job: 'Développeur backend NodeJS',
        portfolio: 'https://danielouis.com',
        email: 'louis_daniel@outlook.fr',
        phone: '+33 7 81 21 25 90',
        gitlab: 'https://gitlab.com/Louisbm',
        linkedin: 'https://www.linkedin.com/in/louis-daniel-860093178',
        driverLicense: true
    },
    professional: [
        {
            type: 'Volontariat',
            jobTitle: 'Bénévole',
            company: 'American Institude of Behavioral Research & Technology',
            startDate: 2022-02-01,
            endDate: null,
            description: 'L'AIBRT est une organisation à but non lucrative, non partisan, créée en 2021 et basée à Vista en Californie aux USA. Elle conduit et promeut des recherches qui ont le potentiel d'augmenter le bien-être de la population sur la planète. Actuellement, AIBRT mène des recherches dans 12 secteurs différents.'
        },
        {
            type: 'Freelance',
            jobTitle: 'Développeur backend NodeJS',
            company: 'soit-même'
            startDate: 2022-01-01,
            endDate: 2022-03-07,
            description: 'Création de sites web pour des entreprises telles que Iso Plac Deco, plaquiste souhaitant refaire son site internet. Principalement avec Javascript (NodeJS) et un peu de VueJS.'
        },
        {
            type: 'CDI',
            jobTitle: 'Fondateur',
            company: 'Speache',
            startDate: 2020-11-01,
            endDate: null,
            description: 'Speache est un service de location basé sur l'économie participative. J'ai crée cette application avec NodeJS, PHP, MySQL et Redis pour le côté serveur.'
        },
        {
            type: 'Apprentissage',
            jobTitle: 'Analyste Quantitatif sur Risque de Crédit IFRS9',
            company: 'BNP Paribas',
            startDate: 2019-09-01,
            endDate: 2020-09-01,
            description: 'Chez BNP Paribas, mes principales missions étaient de produire les pertes attendues (provisions) sur le périmètre Outre-Mer. Pour cela, j'ai utilisé SAS et SQL afin de réaliser des analyses sur les bases de données de l'entreprise. J'ai crée et maintenu des programmes existants, crée la documentation associée et plus encore.'
        },
        {
            type: 'Volontariat',
            jobTitle: 'President',
            company: 'Association Étudiante Rennaise des Ingénieurs Économiques et Financiers (AERIEF)',
            startDate: 2019-09-01,
            endDate: 2020-09-01,
            description: 'En tant que président de l'association, j'ai organisé plusieurs évènements, été le lien entre les étudiants, les enseignants ainsi que les professionnels et réalisé plusieurs démarches administratives.'
        },
        {
            type: 'Stage',
            jobTitle: 'Accompagnateur du développement et de la performance',
            company: 'Société Générale',
            startDate: '2019-06-01',
            endDate: '2019-08-31',
            description: 'Analyse statistique des opérations commerciales, communication interne de la performance de 95 collaborateurs, développement VBA, assistance de la force de vente...'
        }
    ],
    academic: {
        degree: 'Master',
        major: 'Ingénierie Économique et Financière',
        university: 'Faculté des sciences économiques - Université de Rennes 1',
        city: 'Rennes',
        country: 'France'
        startDate: '2018-09-01',
        endDate: '2020-09-01'
    },
    languages: [
        {
            name: 'Français',
            level: '100%'
        },
        {
            name: 'Anglais',
            level: 80%
        },
        {
            name: 'Bulu',
            level: '50%'
        },
        {
            name: 'Espagnol',
            level: '25%'
        }
    ],
    hobbies: [
        {
            topic: 'Sport',
            description: 'Football, Cardio training'
        },
        {
            topic: 'Nouvelles technologies',
            description: 'VR, AR, Fintech'
        },
        {
            topic: 'Développement personnel',
            description: 'Dale Carnegie : How to develop self confidence and influence people by public speaking'
        },
        {
            topic: 'Mangas',
            description: 'J'aime principalement regarder des animes, comme par exemple Shingeki No Kyojin, Demon Slayer pour citer les plus connus.'
        }
    ]       
}


## GET       /api/v1/cv/about
{
    status: 'success',
    data: {
        about: {
            firstName: 'Louis',
            lastName: 'DANIEL',
            job: 'Backend Developer NodeJS',
            portfolio: 'https://danielouis.com',
            email: '',
            phone: '',
            gitlab: '',
            linkedin: '',
            driverLicense: true
        }
    }
}


## GET       /api/v1/cv/professional
{
    status: 'success',
    data: {
        professionnal: [
            {
                type: 'Volunteering',
                jobTitle: 'Volunteer',
                company: 'American Institude of Behavioral Research & Technology',
                startDate: '',
                endDate: '',
                description: ''
            },
            {
                type: 'Freelance',
                jobTitle: 'Backend Developer NodeJS',
                company: 'self'
                startDate: '',
                endDate: '',
                description: 'Missions + technos',
            },
            {
                type: 'CDI',
                jobTitle: 'Founder',
                company: 'Speache',
                startDate: '',
                endDate: '',
                description: ''
            },
            {
                type: 'Apprenticeship',
                jobTitle: 'Quantitative Analyst in Credit Risk IFRS9',
                company: 'BNP Paribas',
                startDate: '',
                endDate: '',
                description: ''
            },
            {
                type: 'Internship',
                jobTitle: '',
                company: 'Societe Generale',
                startDate: '',
                endDate: '',
                description: ''
            }
        ]
    }
}


## GET       /api/v1/cv/academic
{
    status: 'success',
    data: {
        academic: {
            degree: 'Master',
            major: 'Ingénierie Économique et Financière',
            university: 'Faculté des sciences économiques - Université de Rennes 1',
            location: 'Rennes'
            startDate: '',
            endDate: '',
            description: ''
        }
    }
}


## GET       /api/v1/cv/techs
{
    status: 'success',
    data: {
        techs: {
            languages: [
                {
                    name: 'MongoDB',
                    level: 50,
                    duration: '',
                    projects: [
                        {
                            title: '',
                            description: ''
                        }
                    ]
                }
            ],
            databases: [
                {
                    name: 'MongoDB',
                    level: 50,
                    duration: '',
                    projects: [
                        {
                            title: '',
                            description: ''
                        }
                    ]
                },
                {
                    name: 'MySQL',
                    level: 80,
                    duration: '',
                    projects: [
                        {
                            title: '',
                            description: ''
                        }
                    ]
                }
            ],
            others: [

            ]
        }
    }
}


## GET       /api/v1/cv/languages
{
    status: 'success',
    data: {
        languages: [
            {
                name: 'French',
                level: 100
            },
            {
                name: 'English',
                level: 80
            },
            {
                name: 'Bulu',
                level: 50
            },
            {
                name: 'Spanish',
                level: 25
            }
        ]
    }
}


## GET       /api/v1/cv/hobbies
{
    status: 'sucess',
    data: {
        hobbies: [
            {
                topic: 'Sport',
                description: 'Football, Cardio training'
            },
            {
                topic: 'New technologies',
                description: ''
            },
            {
                topic: 'Personal development',
                description: ''
            },
            {
                topic: 'Mangas',
                description: ''
            }
        ]  
    }
}