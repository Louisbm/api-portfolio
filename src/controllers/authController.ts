const jwt = require('jsonwebtoken');
const bcrypt = require('bcrypt');

const db = require('./../db/postgre');

import type { Request, Response, NextFunction, CookieOptions } from 'express';
import type { SignOptions, Jwt, JwtPayload } from 'jsonwebtoken';
import type { Error } from './../types/error';
import type { User } from './../types/user';

exports.isLoggedIn = async (req: Request, res: Response, next: NextFunction): Promise<void> => {

    try {

        const token: Jwt = req.cookies.jwt;

        let error: Error = {
            message: '',
            status: 'fail',
            statusCode: 401,
            isClient: true,
        };

        if (!token) {
            error.message = 'Veuillez vous authentifier pour continuer!';
            return next(error);
        }

        const decodedUser: JwtPayload = await jwt.verify(token, process.env.JWT_SECRET);
        
        //check if user still exists
        const query: string = 'SELECT id, email, password FROM users WHERE id = $1';
        const params: number[] = [decodedUser.id];

        const user: User = (await db.query(query, params)).rows[0];

        if (!user) {
            error.message = `L'utilisateur n'existe plus!`;
            return next(error);
        }

        next();

    } catch (err) {

        let error: Error = {
            message: '',
            status: 'fail',
            statusCode: 500,
            isServer: true
        };

        if (req.lang === 'fr') {
            error.message = `Oups, quelque chose s'est mal passé!`;
            return next(error);
        }
        
        error.message = 'Oops, something went very wrong!';
        return next(error);

    }

};

exports.login = async (req: Request, res: Response, next: NextFunction) => {

    try {

        const { email, password }: { email?: string, password?: string } = req.body;

        let error: Error = {
            message: '',
            status: 'fail',
            statusCode: 0,
            isClient: true
        };

        if (!email || !email.trim() || !password || !password.trim()) {
            error.message = 'Please, provide an email and a password.';
            error.statusCode = 400;
            return next(error);
        }

        const query: string = 'SELECT id, email, password FROM users WHERE email = $1';
        const params: string[] = [email];

        const user: User = (await db.query(query, params)).rows[0];

        if (!user) {
            error.message = 'Email or password incorrect!';
            error.statusCode = 401;
            return next(error);
        }

        const isPassword: boolean = await bcrypt.compare(password, user.password);

        if (!isPassword) {
            error.message = 'Email or password incorrect!';
            error.statusCode = 401;
            return next(error);
        }

        const jwtPayload: { id: number } = { id: user.id as number };
        const jwtSecret: string = process.env.JWT_SECRET as string;
        const jwtOptions: SignOptions = { expiresIn: process.env.JWT_EXPIRES_IN };

        const token: Jwt = jwt.sign(jwtPayload, jwtSecret, jwtOptions);

        const cookieExpiresIn: number = parseInt(process.env.JWT_COOKIE_EXPIRES_IN as string);

        const cookieOptions: CookieOptions = {
            expires: new Date(Date.now() + cookieExpiresIn * 24 * 60 * 60 * 1000),
            httpOnly: true,
            sameSite: 'lax'
            //secure: true
        };

        res.cookie('jwt', token, cookieOptions);

        res.status(200).json({
            status: 'success',
            data: {
                message: 'Vous êtes connecté!'
            }
        });

    } catch (err) {

        let error: Error = {
            message: '',
            status: 'fail',
            statusCode: 500,
            isServer: true
        };

        if (req.lang === 'fr') {
            error.message = `Oups, quelque chose s'est mal passé!`;
            return next(error);
        }
        
        error.message = 'Oops, something went very wrong!';
        return next(error);

    }

};

exports.logout = (req: Request, res: Response) => {

    res.clearCookie('jwt', {
        httpOnly: true,
        sameSite: 'lax'
    });

    res.status(200).json({
        status: 'success',
        data: {
            message: 'Vous êtes déconnecté!'
        }
    });

};