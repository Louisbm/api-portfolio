const db = require('./../db/postgre');

import type { Request, Response, NextFunction } from 'express';
import type { Error } from './../types/error';

exports.getAllTechnos = async (req: Request, res: Response, next: NextFunction) => {

    try {
        
        const query: string = `SELECT id, name FROM technologies`;

        const technos: { [key: string]: string | number }[] = (await db.query(query)).rows;

        res.status(200).json({
            status: 'success',
            data: {
                technos
            }
        });
        
    } catch (err) {

        let error: Error = {
            message: `Oups, quelque chose s'est mal passé!`,
            status: 'fail',
            statusCode: 500,
            isServer: true
        };

        return next(error);

    }

};