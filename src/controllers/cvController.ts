const db = require('./../db/postgre');

import type { Request, Response, NextFunction } from 'express';
import type { Error } from './../types/error';

exports.getAllCv = async (req: Request, res: Response, next: NextFunction) => {

    try {

        const { lang }: { lang: string } = req;

        const query: string = `SELECT ${lang} AS cv FROM cv`;

        const cv: {} = (await db.query(query)).rows[0].cv;

        res.status(200).json({
            status: 'success',
            data: {
                cv
            }
        });

    } catch (err) {

        let error: Error = {
            message: `Oups, quelque chose s'est mal passé!`,
            status: 'fail',
            statusCode: 500,
            isServer: true
        };

        return next(error);

    }

};

exports.getCvAbout = async (req: Request, res: Response, next: NextFunction) => {

    try {

        const { lang }: { lang: string } = req;

        const query: string = `SELECT ${lang} -> 'about' AS about FROM cv`;

        const about: {} = (await db.query(query)).rows[0].about;

        res.status(200).json({
            status: 'success',
            data: {
                about
            }
        });

    } catch (err) {

        let error: Error = {
            message: `Oups, quelque chose s'est mal passé!`,
            status: 'fail',
            statusCode: 500,
            isServer: true
        };

        return next(error);

    }

};

exports.getCvProfessional = async (req: Request, res: Response, next: NextFunction) => {

    try {

        const { lang }: { lang: string } = req;

        const query: string = `SELECT ${lang} -> 'professional' AS professional FROM cv`;

        const professional: {} = (await db.query(query)).rows[0].professional;

        res.status(200).json({
            status: 'success',
            data: {
                professional
            }
        });

    } catch (err) {

        let error: Error = {
            message: `Oups, quelque chose s'est mal passé!`,
            status: 'fail',
            statusCode: 500,
            isServer: true
        };

        return next(error);

    }

};

exports.getCvAcademic = async (req: Request, res: Response, next: NextFunction) => {

    try {

        const { lang }: { lang: string } = req;

        const query: string = `SELECT ${lang} -> 'academic' AS academic FROM cv`;

        const academic: {} = (await db.query(query)).rows[0].academic;

        res.status(200).json({
            status: 'success',
            data: {
                academic
            }
        });

    } catch (err) {

        let error: Error = {
            message: `Oups, quelque chose s'est mal passé!`,
            status: 'fail',
            statusCode: 500,
            isServer: true
        };

        return next(error);

    }

};

exports.getCvLanguages = async (req: Request, res: Response, next: NextFunction) => {

    try {

        const { lang }: { lang: string } = req;

        const query: string = `SELECT ${lang} -> 'languages' AS languages FROM cv`;

        const languages: {} = (await db.query(query)).rows[0].languages;

        res.status(200).json({
            status: 'success',
            data: {
                languages
            }
        });

    } catch (err) {

        let error: Error = {
            message: `Oups, quelque chose s'est mal passé!`,
            status: 'fail',
            statusCode: 500,
            isServer: true
        };

        return next(error);

    }

};

exports.getCvHobbies = async (req: Request, res: Response, next: NextFunction) => {

    try {

        const { lang }: { lang: string } = req;

        const query: string = `SELECT ${lang} -> 'hobbies' AS hobbies FROM cv`;

        const hobbies: {} = (await db.query(query)).rows[0].hobbies;

        res.status(200).json({
            status: 'success',
            data: {
                hobbies
            }
        });

    } catch (err) {

        let error: Error = {
            message: `Oups, quelque chose s'est mal passé!`,
            status: 'fail',
            statusCode: 500,
            isServer: true
        };

        return next(error);

    }

};