const db = require('./../db/postgre');

import type { Request, Response, NextFunction } from 'express';
import type { Error } from './../types/error';
import type { ProjectsList, Project, ProjectFields } from './../types/projects';


exports.getAllProjects = async (req: Request, res: Response, next: NextFunction) => {
    
    try {

        const { lang }: { lang: string } = req;

        const query: string = `
            SELECT 
                t1.id, 
                t1.title_${lang} AS title, 
                t1.description_${lang} AS description, 
                t1.url, 
                t1.repository,
                ARRAY_AGG(t3.name) AS technos
            FROM 
                projects t1
            LEFT JOIN map_projects_technologies t2
            ON t1.id = t2.project_id
            LEFT JOIN technologies t3
            ON t3.id = t2.technology_id
            GROUP BY t1.id
        `;

        const projects: ProjectsList = (await db.query(query)).rows;
        
        res.status(200).json({
            status: 'success',
            data: {
                projectsNumber: projects.length,
                projects
            }
        });

    } catch(err) {
        
        let error: Error = {
            message: '',
            status: 'fail',
            statusCode: 500,
            isServer: true
        };

        if (req.lang === 'fr') {
            error.message = `Oups, quelque chose s'est mal passé!`;
            return next(error);
        }
        
        error.message = 'Oops, something went very wrong!';
        return next(error);

    }

};

exports.getProjectId = (req: Request, res: Response, next: NextFunction): void => {

    if (/^\d+$/.test(req.params.id)) {
        req.projectId = req.params.id;
        return next();
    }

    let error: Error = {
        message: '',
        status: 'fail',
        statusCode: 400,
        isClient: true
    };

    if (req.lang === 'fr') {
        //new clientError({ 400, `Veuillez insérer l'id d'un projet!` });
        error.message = `Veuillez insérer l'id d'un projet!`;
        return next(error);
    }

    error.message = 'Please, specify an id for the project!';
    return next(error);
};

exports.getProject = async (req: Request, res: Response, next: NextFunction) => {

    try {

        const { lang, projectId }: { lang: string, projectId: string } = req;

        const query: string = `
            SELECT
                t1.id,
                t1.title_${lang} AS title,
                t1.description_${lang} AS description,
                t1.url,
                t1.repository,
                ARRAY_AGG(t3.name) AS technos
            FROM
            projects t1
            INNER JOIN map_projects_technologies t2
            ON t1.id = t2.project_id
            INNER JOIN technologies t3
            ON t3.id = t2.technology_id
            WHERE t1.id = $1
            GROUP BY t1.id
        `;
        const params = [projectId];

        const project: Project = (await db.query(query, params)).rows[0];

        if (!project) {

            const messageFr: string = 'Cet id ne correspond à aucun projet!';
            const messageEn: string = `This id doen't belong to any project!`;

            const message: string = (lang === 'fr') ? messageFr : messageEn;

            let error: Error = {
                message: message,
                status: 'fail',
                statusCode: 400,
                isClient: true
            };

            return next(error);
        }

        res.status(200).json({
            status: 'success',
            data: {
                project
            }
        });

    } catch(err) {
        
        let error: Error = {
            message: '',
            status: 'fail',
            statusCode: 500,
            isServer: true
        };

        if (req.lang === 'fr') {
            error.message = `Oups, quelque chose s'est mal passé!`;
            return next(error);
        }
        
        error.message = 'Oops, something went very wrong!';
        return next(error);

    }

};

exports.createProject = async (req: Request, res: Response, next: NextFunction) => {

    try {

        const {
            title_en,
            title_fr,
            description_en,
            description_fr,
            url,
            repository,
            technos 
        }: ProjectFields = req.body;

        let error: Error = {
            message: {},
            status: 'fail',
            statusCode: 0,
            isClient: true
        };

        if (typeof error.message !== 'object') {
            error.message = `Oups, quelque chose s'est mal passé!`;
            error.statusCode = 400;
            return next(error);
        }

        if (!title_en || !title_en.trim()) {
            error.message.title_en = 'Veuillez insérer un titre en anglais!';
        }

        if (!title_fr || !title_fr.trim()) {
            error.message.title_fr = 'Veuillez insérer un titre en français!';
        }

        if (!description_en || !description_en.trim()) {
            error.message.description_en = 'Veuillez insérer une description en anglais!';
        }

        if (!description_fr || !description_fr.trim()) {
            error.message.description_fr = 'Veuillez insérer une description en français!';
        }

        if ((!url || !url.trim()) && url !== null) {
            error.message.url = 'Veuillez indiquer un url ou null sinon!';
        }

        if ((!repository || !repository.trim()) && repository !== null) {
            error.message.repository = 'Veuillez indiquer un repository ou null sinon!';
        }

        if (!technos || !Array.isArray(technos) || technos.length === 0) {
            error.message.technos = 'Veuillez indiquer une liste de technologies!';
        }

        if (Object.keys(error.message).length > 0) {
            error.statusCode = 400;
            return next(error);
        }

        const queryInsertProject: string = `
            INSERT INTO projects 
            (
                title_en,
                title_fr,
                description_en,
                description_fr,
                url,
                repository
            ) VALUES ($1, $2, $3, $4, $5, $6) RETURNING id
        `;

        const paramsInsertProject: (string | null)[] = [title_en, title_fr, description_en, description_fr, url, repository] as (string | null)[];

        const insertedId: number = (await db.query(queryInsertProject, paramsInsertProject)).rows[0].id;

        const queryInsertTechnos: string = `
            INSERT INTO map_projects_technologies (project_id, technology_id)
            VALUES (${insertedId}, UNNEST($1::integer[]))
        `;

        const insertedTechnos: number = (await db.query(queryInsertTechnos, [technos])).rowCount;

        res.status(200).json({
            status: 'success',
            data: {
                message: 'Projet publié!',
                insertedTechnos
            }
        });

    } catch (err) {
        
        let error: Error = {
            message: `Oups, quelque chose s'est mal passé!`,
            status: 'fail',
            statusCode: 500,
            isServer: true
        };

        return next(error);
        
    }

};

exports.updateProject = async (req: Request, res: Response, next: NextFunction) => {
    
    try {

        const { projectId }: { projectId: string } = req;
        const { 
            title_fr, 
            title_en, 
            description_fr, 
            description_en, 
            url, 
            repository, 
            technos 
        }: ProjectFields = req.body;

        let error: Error = {
            message: {},
            status: 'fail',
            statusCode: 0,
            isClient: true
        };

        if (typeof error.message !== 'object') {
            error.message = `Oups, quelque chose s'est mal passé!`;
            error.statusCode = 400;
            return next(error);
        }

        if (!title_en || !title_en.trim()) {
            error.message.title_en = 'Veuillez insérer un titre en anglais!';
        }

        if (!title_fr || !title_fr.trim()) {
            error.message.title_fr = 'Veuillez insérer un titre en français!';
        }

        if (!description_en || !description_en.trim()) {
            error.message.description_en = 'Veuillez insérer une description en anglais!';
        }

        if (!description_fr || !description_fr.trim()) {
            error.message.description_fr = 'Veuillez insérer une description en français!';
        }

        if ((!url || !url.trim()) && url !== null) {
            error.message.url = 'Veuillez indiquer un url ou null sinon!';
        }

        if ((!repository || !repository.trim()) && repository !== null) {
            error.message.repository = 'Veuillez indiquer un repository ou null sinon!';
        }

        if (!technos || !Array.isArray(technos) || technos.length === 0) {
            error.message.technos = 'Veuillez indiquer une liste de technologies!';
        }

        if (Object.keys(error.message).length > 0) {
            error.statusCode = 400;
            return next(error);
        }

        const queryUpdateProject: string = `
            UPDATE projects
            SET
                title_fr = $1,
                title_en = $2,
                description_fr = $3,
                description_en = $4,
                url = $5,
                repository = $6
            WHERE id = $7
            RETURNING id
        `;

        const paramsUpdateProject: (string | null)[] = [title_fr, title_en, description_fr, description_en, url, repository, projectId] as (string | null)[];

        const updatedId: number | undefined = (await db.query(queryUpdateProject, paramsUpdateProject)).rows[0]?.id;

        if (!updatedId) {
            let error: Error = {
                message: `Le projet n'existe pas!`,
                status: 'fail',
                statusCode: 400,
                isClient: true
            };

            return next(error);
        }

        const queryDeleteTechnos: string = `
            DELETE FROM map_projects_technologies WHERE project_id = $1 RETURNING project_id, technology_id
        `;
        const paramsDeleteTechnos: string[] = [projectId];

        const deletedIds: [] | { [key: string]: number }[] = (await db.query(queryDeleteTechnos, paramsDeleteTechnos)).rows;

        const queryInsertTechnos: string = `
            INSERT INTO map_projects_technologies (project_id, technology_id)
            VALUES (${projectId}, UNNEST($1::integer[]))
        `;

        const insertedTechnos: number = (await db.query(queryInsertTechnos, [technos])).rowCount;

        res.status(200).json({
            status: 'success',
            data: {
                message: `Le projet avec l'id ${updatedId} a été mis à jour!`,
            }
        });


    } catch (err) {

        console.log(err);

        let error: Error = {
            message: `Oups, quelque chose s'est mal passé!`,
            status: 'fail',
            statusCode: 500,
            isServer: true
        };

        return next(error);

    }

};

exports.deleteProject = async (req: Request, res: Response, next: NextFunction) => {

    try {

        const { projectId }: { projectId: string } = req;

        const query: string = `DELETE FROM projects WHERE id = $1 RETURNING id`;

        const params: string[] = [projectId];

        const deletedId: number | undefined = (await db.query(query, params)).rows[0]?.id;

        if (!deletedId) {

            let error: Error = {
                message: `Le projet n'existe pas!`,
                status: 'fail',
                statusCode: 400,
                isClient: true
            };

            return next(error);
        }

        res.status(200).json({
            status: 'success',
            data: {
                message: `Le projet avec l'id ${deletedId} a bien été supprimé!`
            }
        });

    } catch (err) {

        let error: Error = {
            message: `Oups, quelque chose s'est mal passé!`,
            status: 'fail',
            statusCode: 500,
            isServer: true
        };

        return next(error);

    }

};