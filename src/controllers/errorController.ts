import type { Request, Response, NextFunction } from 'express';
import type { Error } from './../types/error';

exports.routeErrors = (req: Request, res: Response) => {
    
    res.status(404).json({
        status: 'fail',
        data: {
            message: `The route ${req.protocol}://${req.headers.host + req.originalUrl} doesn't exists on this server!`
        }
    });

};

exports.logErrors = (err: Error, req: Request, res: Response, next: NextFunction) => {
    //write request and error information to stderr
    // console.error(err.stack);
    console.error('Erreur :\n', err);
    next(err);
};

exports.clientErrors = (err: Error, req: Request, res: Response, next: NextFunction) => {

    if (!err.isClient) return next(err);

    err.statusCode = err.statusCode || 400;
    err.status = err.status || 'fail';

    res.status(err.statusCode).json({
        status: err.status,
        data: {
            message: err.message
        }
    });

};

exports.serverErrors = (err: Error, req: Request, res: Response, next: NextFunction) => {

    err.statusCode = err.statusCode || 500;
    err.status = err.status || 'error';

    res.status(err.statusCode).json({
        status: err.status,
        data: {
            message: err.message
        }
    });

};