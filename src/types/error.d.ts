export type Error = {
    message: string | { [key: string]: string };
    status: string;
    statusCode: number;
    isClient?: boolean;
    isServer?: boolean;
};