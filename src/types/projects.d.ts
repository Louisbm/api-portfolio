export type ProjectsList = {
    [key: string]: string | number | string[] | null 
}[];

export type Project = { [key: string]: number | string | string[] | null } | {};

export type ProjectFields = {
    title_en?: string;
    title_fr?: string;
    description_en?: string;
    description_fr?: string;
    url?: string | null;
    repository?: string | null;
    technos?: number[];
};