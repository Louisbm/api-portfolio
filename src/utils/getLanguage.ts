import type { Request, Response, NextFunction } from 'express';

declare module 'express' {
    interface Request {
        lang: string;
        projectId: string;
    }
}

const getLanguage = (req: Request, res: Response, next: NextFunction): void => {

    if (req.params.lang.toLowerCase() === 'fr') {
        req.lang = 'fr';
        return next();
    }

    req.lang = 'en';
    return next();
};

module.exports = getLanguage;