export {};

const express = require('express');

const cvController = require('./../controllers/cvController');
const getLanguage = require('./../utils/getLanguage');

const router = express.Router();

router.use('/:lang', getLanguage);

router.get('/:lang', cvController.getAllCv);
router.get('/:lang/about', cvController.getCvAbout);
router.get('/:lang/professional', cvController.getCvProfessional);
router.get('/:lang/academic', cvController.getCvAcademic);
router.get('/:lang/languages', cvController.getCvLanguages);
router.get('/:lang/hobbies', cvController.getCvHobbies);

module.exports = router;