export {};

const express = require('express');

const getLanguage = require('./../utils/getLanguage');
const projectController = require('./../controllers/projectController');
const authController = require('./../controllers/authController');

const router = express.Router();

router.get('/:lang', getLanguage, projectController.getAllProjects);
router.get('/:lang/:id', getLanguage, projectController.getProjectId, projectController.getProject);
router.post('/', authController.isLoggedIn, projectController.createProject);

router.route('/:id')
    .patch(projectController.getProjectId, projectController.updateProject)
    .delete(projectController.getProjectId, projectController.deleteProject);

module.exports = router;