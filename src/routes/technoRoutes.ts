export {};

const express = require('express');

const authController = require('./../controllers/authController');
const technoController = require('./../controllers/technoController');

const router = express.Router();

router.get('/', authController.isLoggedIn, technoController.getAllTechnos);

module.exports = router;