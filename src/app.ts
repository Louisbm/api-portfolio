const express = require('express');
const dotenv = require('dotenv');
const cookieParser = require('cookie-parser');

dotenv.config({ path: `${__dirname}/../config.env` });

const projectRouter = require('./routes/projectRoutes');
const userRouter = require('./routes/userRoutes');
const technoRouter = require('./routes/technoRoutes');
const cvRouter = require('./routes/cvRoutes');
const { routeErrors, logErrors, clientErrors, serverErrors } = require('./controllers/errorController');

const app = express();

app.use(express.json({ limit: '10kb' }));
app.use(express.urlencoded({ extended: true, limit: '10kb' }));
app.use(cookieParser());

app.use('/api/v1/projects', projectRouter);
app.use('/api/v1/users', userRouter);
app.use('/api/v1/technos', technoRouter);
app.use('/api/v1/cv', cvRouter);

app.all('*', routeErrors);
app.use(logErrors);
app.use(clientErrors);
app.use(serverErrors);

const port = process.env.PORT || 3000;

app.listen(port, () => {
    console.log(`Server listening on port ${port}`)
});